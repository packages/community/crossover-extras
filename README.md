## Meta package

The package install all libraries (except AUR) listed as optional dependencies for CrossOver Linux.

Use this package if you want a convenient installation of packages which may or may not be needed by a Windows application.
